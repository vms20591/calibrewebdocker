FROM janeczku/calibre-web
RUN \
  curl -L -o /tmp/calibre-library.tar.gz https://www.dropbox.com/s/th5baqvzkg6p5vc/calibre-library.tar.gz?dl=1 && \
  tar -xvf /tmp/calibre-library.tar.gz && \
  mv calibre-library/* /config && \
  rm -rf calibre-library
